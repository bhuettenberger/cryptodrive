﻿using FileAccessLayer;
using FileAccessLayer.XmlObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class DataManager: IDataManager
    {
        public IDriveManager DriveManager { get; set; }
        public ICryptology CryptoManager { get; set; }
        public IFileManager FileManager { get; set; }
        public IXmlManager XmlManager { get; set; }
        public event ErrorAction ErrorMessage;
        public event ProgressAction Progress;

        public DataManager() { }

        public DataManager(IDriveManager DriveManager, ICryptology CryptoManager, 
            IFileManager FileManager, IXmlManager XmlManager)
        {
            this.DriveManager = DriveManager;
            this.CryptoManager = CryptoManager;
            this.FileManager = FileManager;
            this.XmlManager = XmlManager;
            DriveManager.Progress += DriveManager_Progress;
        }

        //fire Progress of Upload
        private void DriveManager_Progress(double percent)
        {
            if (Progress != null)
            {
                Progress(percent);
            }
        }

        public void CreateBackup(DirectoryInfo dInfo, string password, string xmlPath)
        {
            string parentId = "";
            string id = "";
            double count = 1;
            double percent;
            Dictionary<string, GoogleFile> searchStructure; 
            List<GoogleFile> googleFiles = new List<GoogleFile>();
            Google.Apis.Drive.v2.Data.File googleFile;

            try
            {
                if (!dInfo.Exists || password.Length < 8)
                {
                    throw new ArgumentException("Eingabeparameter ungültig! Bitte überprüfen Sie die Passwortlänge und den Dateipfad");
                }

                FileManager.dInfo = dInfo;
                List<FileSystemInfo> fileStructure = FileManager.GetAllFilesOfDirectoryWithFolders();

                //get first element of backup.xml and compare it with folder of filestructure
                //if folders are not equal create a new backup
                if(CheckIfBackupAlreadyExists(fileStructure.First().FullName, ref xmlPath))
                {
                    googleFiles = XmlManager.DeserializeFromXml(xmlPath);

                    searchStructure = InitializeSearchStructure(googleFiles);

                    foreach (var fileItem in fileStructure)
                    {
                        percent = (count / fileStructure.Count) * 100;
                        parentId = GetParentId(fileItem, dInfo, searchStructure);

                        if (CheckIfFileAlreadyUploaded(fileItem, searchStructure))
                        {
                            if (NeedsUpdate(fileItem, searchStructure, ref id))
                            {
                                DriveManager.UpdateFileInDrive(id, (FileInfo)fileItem, password);
                                //change created date in google file
                                UpdateListOfFiles(id, googleFiles, (FileInfo)fileItem);
                            }
                        }
                        else //new file add it to backup folder
                        {
                            if (fileItem.Attributes.ToString().IndexOf("Directory") != -1) //create folder
                            {
                                googleFile = DriveManager.CreateFolderInDrive(parentId, (DirectoryInfo)fileItem);
                            }
                            else //create file
                            {
                                googleFile = DriveManager.UploadFileAddToBackupDirectory(parentId, (FileInfo)fileItem, password);
                            }
                            googleFiles.Add(new GoogleFile()
                            {
                                Id = googleFile.Id,
                                Created = googleFile.CreatedDate,
                                Name = googleFile.Title,
                                FilePath = fileItem.FullName + "_encrypted",
                                DownloadUrl = googleFile.DownloadUrl
                            });
                        }
                        DriveManager_Progress(percent);
                        count++;
                    }
                    XmlManager.SerializeToXml(xmlPath, googleFiles);
                }
                else //create new backup and another xml file
                {
                    CreateNewBackup(fileStructure, googleFiles, password, xmlPath);
                }
            }
            catch(Exception e)
            {
                if (e.Message == "Error:\"invalid_grant\", Description:\"\", Uri:\"\"")
                {
                    ErrorMessage("Authentifizierung an Google Drive fehlgeschlagen. Starten Sie den Vorgang neu und registrieren Sie den Zugriff!");
                }
                else if(this.ErrorMessage != null)
                {
                    ErrorMessage(e.Message);
                }
            }
        }

        //check if there is already a proper folder
        private bool CheckIfBackupAlreadyExists(string filepath, ref string xmlPath)
        {
            FileInfo firstFile = new FileInfo(filepath);
            FileInfo xmlFile = new FileInfo(xmlPath);
            DirectoryInfo fileFolder = firstFile.Directory;
            DirectoryInfo xmlFolder = xmlFile.Directory;
            List<FileInfo> backupFiles = GetAllFileInfosOfFolder(xmlFolder);

            foreach (var backupFile in backupFiles)
            {
                var gFiles = XmlManager.DeserializeFromXml(backupFile.FullName);

                //compare folder of backupfiles with root folder of xml backup files
                if (fileFolder.FullName == gFiles.First().FilePath)
                {
                    xmlPath = backupFile.FullName;
                    return true;
                }
            }

            return false;
        }

        //create a new backup on google drive
        private void CreateNewBackup(List<FileSystemInfo> fileStructure, List<GoogleFile> googleFiles, string password, string xmlPath)
        {
            double count = 1;
            var gFiles = DriveManager.UploadFilesWithDirectories(fileStructure, password);

            foreach (var gFile in gFiles)
            {
                googleFiles.Add(new GoogleFile()
                {
                    Id = gFile.Value.Id,
                    Created = gFile.Value.CreatedDate,
                    Name = gFile.Value.Title,
                    FilePath = gFile.Key,
                    DownloadUrl = gFile.Value.DownloadUrl
                });
                count++;
            }
            XmlManager.SerializeToXml(xmlPath, googleFiles);
        }

        //updates the creation date in the updatedfile
        private void UpdateListOfFiles(string id, List<GoogleFile> googleFiles, FileInfo fileInfo)
        {
            foreach (var gFile in googleFiles)
            {
                if(gFile.Id == id)
                {
                    gFile.Created = fileInfo.LastWriteTime;
                }
            }
        }

        //get parent id of file object
        private string GetParentId(FileSystemInfo fileItem, DirectoryInfo dInfo, Dictionary<string, GoogleFile> searchStructure)
        {
            if (fileItem.Attributes.ToString().IndexOf("Directory") != -1)
            {
                DirectoryInfo tempInfo = (DirectoryInfo)fileItem;
                if(tempInfo.Parent.FullName == dInfo.FullName) //rootpath
                {
                    return searchStructure.First().Value.Id;
                }
                return searchStructure[tempInfo.Parent.FullName].Id;
            }
            else
            {
                FileInfo tempInfo = (FileInfo)fileItem;
                if (tempInfo.DirectoryName == dInfo.FullName) //rootpath
                {
                    return searchStructure.First().Value.Id;
                }
                return searchStructure[tempInfo.DirectoryName].Id;
            }
        }

        //initializes search strucuture for filesearch
        private Dictionary<string, GoogleFile> InitializeSearchStructure(List<GoogleFile> googleFiles)
        {
            Dictionary<string, GoogleFile> dict = new Dictionary<string, GoogleFile>();

            foreach (var gFile in googleFiles)
            {
                dict.Add(gFile.FilePath, gFile);
            }

            return dict;
        }

        //check if file already exists in backup folder
        private bool CheckIfFileAlreadyUploaded(FileSystemInfo fInfo, Dictionary<string, GoogleFile> gFiles)
        {
            if (gFiles.ContainsKey(fInfo.FullName) || gFiles.ContainsKey(fInfo.FullName + "_encrypted"))
            {
                return true;
            }
            return false;
        }

        //check if file needs to be Updated
        private bool NeedsUpdate(FileSystemInfo fInfo, Dictionary<string, GoogleFile> gFiles, ref string id)
        {
            //folders don't need to be updated
            if (fInfo.Attributes.ToString().IndexOf("Directory") == -1)
            {
                if(gFiles[fInfo.FullName + "_encrypted"].Created < fInfo.LastWriteTime)
                {
                    id = gFiles[fInfo.FullName + "_encrypted"].Id;
                    return true;
                }
            }
            return false;
        }

        public void RestoreBackup(DirectoryInfo dInfo, string password, string xmlPath)
        {
            double count = 1;
            double percent;
            string rootPath = "";
            string directoryPath = "";
            string filePath = "";
            List<GoogleFile> googleFiles = new List<GoogleFile>();

            try
            {
                if (!dInfo.Exists || password.Length < 8)
                {
                    throw new ArgumentException("Eingabeparameter ungültig! Bitte überprüfen Sie die Passwortlänge und den Dateipfad");
                }
                //Deserialize XML
                googleFiles = XmlManager.DeserializeFromXml(xmlPath);

                //Get Root Path
                rootPath = googleFiles.First().FilePath;

                foreach (var gFile in googleFiles)
                {
                    percent = (count / googleFiles.Count)*100;
                    if (gFile.DownloadUrl != null) //create file
                    {
                        filePath = gFile.FilePath;

                        //Replace every Root path with new path
                        filePath = filePath.Replace(rootPath, dInfo.FullName);
                        var stream = DriveManager.DownloadFile(gFile.Id);
                        FileManager.CreateFileFromStream(filePath, stream);

                        //decrypt file
                        var fInfo = CryptoManager.DecryptFile(new FileInfo(filePath), password);

                        if(fInfo != null)
                        {
                            FileManager.DeleteFile(filePath);
                        } 
                    }
                    else //create folder
                    {
                        directoryPath = gFile.FilePath;
                        //Replace every Root path with new path
                        directoryPath = directoryPath.Replace(rootPath, dInfo.FullName);
                        FileManager.CreateDirectory(directoryPath);
                    }
                    //fire progress
                    DriveManager_Progress(percent);
                    count++;
                }
            }
            catch(Exception e)
            {
                if (e.Message == "Error:\"invalid_grant\", Description:\"\", Uri:\"\"")
                {
                    ErrorMessage("Authentifizierung an Google Drive fehlgeschlagen. Starten Sie den Vorgang neu und registrieren Sie den Zugriff");
                }
                else if (this.ErrorMessage != null)
                {
                    ErrorMessage(e.Message);
                }
            }
        }

        public List<KeyValuePair<string,string>> GetAllFilesOfFolder(DirectoryInfo dInfo)
        {
            List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
            List<FileInfo> backups = FileManager.GetAllFilesOfDirectory(dInfo);

            foreach (var backupFile in backups)
            {
                var gFiles = XmlManager.DeserializeFromXml(backupFile.FullName);
                string folder = gFiles.First().FilePath;
                int position = folder.LastIndexOf("\\");
                folder = folder.Substring(position + 1);
                list.Add(new KeyValuePair<string, string>(backupFile.FullName, folder));
            }

            return list;
        }

        //get all fileinfos of folder
        private List<FileInfo> GetAllFileInfosOfFolder(DirectoryInfo dInfo)
        {
            return FileManager.GetAllFilesOfDirectory(dInfo);
        }


        public void RevokeAccessOfApplication()
        {
            DriveManager.RevokeAccessToken();
        }
    }
}
