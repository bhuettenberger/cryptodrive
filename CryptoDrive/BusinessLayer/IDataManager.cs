﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public delegate void ProgressAction(double percent);
    public delegate void ErrorAction(string message);
    public interface IDataManager
    {
        event ErrorAction ErrorMessage;
        event ProgressAction Progress;
        /// <summary>
        /// Creates a backup of the specified folder in google drive
        /// </summary>
        /// <param name="dInfo"></param>
        /// <param name="password"></param>
        /// <param name="xmlPath"></param>
        void CreateBackup(DirectoryInfo dInfo, string password, string xmlPath);

        /// <summary>
        /// Restores the backup folder from google drive to directory
        /// </summary>
        /// <param name="dInfo">Directory where the backup needs to be restored</param>
        /// <param name="password">Password for decryption</param>
        /// <param name="xmlPath">Path to list of stored files in google drive</param>
        void RestoreBackup(DirectoryInfo dInfo, string password, string xmlPath);

        /// <summary>
        /// Get all backupdirectories from a folder
        /// </summary>
        /// <param name="dInfo">folder with files</param>
        /// <returns>only files without files from subfolder</returns>
        List<KeyValuePair<string, string>> GetAllFilesOfFolder(DirectoryInfo dInfo);

        /// <summary>
        /// Revokes the access of CryptoDrive to Google Drive
        /// </summary>
        void RevokeAccessOfApplication();
    }
}
