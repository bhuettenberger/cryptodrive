﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileAccessLayer
{
    public interface IFileManager
    {
        /// <summary>
        /// needs to be set ;)
        /// </summary>
        DirectoryInfo dInfo { get; set; }

        /// <summary>
        /// Get only files from directory without folders or subfolders
        /// </summary>
        /// <param name="dInfo">directory</param>
        /// <returns>List of files in directory</returns>
        List<FileInfo> GetAllFilesOfDirectory(DirectoryInfo dInfo);

        /// <summary>
        /// Reads all files of directory with subfiles
        /// </summary>
        /// <returns>List of all files in Directory</returns>
        List<FileInfo> GetAllFilesOfDirectory();

        /// <summary>
        /// Reads all files of directory with subfiles including the directory structure
        /// </summary>
        /// <returns></returns>
        List<FileSystemInfo> GetAllFilesOfDirectoryWithFolders();

        /// <summary>
        /// Creates the file out of a stream
        /// </summary>
        /// <param name="stream">file stream</param>
        void CreateFileFromStream(string filePath, Stream stream);

        /// <summary>
        /// Creates a directory in Path
        /// </summary>
        /// <param name="directoryPath"></param>
        void CreateDirectory(string directoryPath);

        /// <summary>
        /// Deletes a file
        /// </summary>
        /// <param name="filePath">full path of file</param>
        void DeleteFile(string filePath);

        /// <summary>
        /// Deletes the google drive access token. The user has to reenter the credentials when he wants to use the application
        /// </summary>
        bool DeleteGoogleAccessToken();
    }
}
