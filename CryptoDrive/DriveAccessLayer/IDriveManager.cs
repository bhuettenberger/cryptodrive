﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Google.Apis.Drive.v2.Data;

namespace FileAccessLayer
{
    public delegate void UploadProgress(double percent);
    public interface IDriveManager
    {
        event UploadProgress Progress;
        /// <summary>
        /// Uploads a single file to google drive
        /// </summary>
        /// <param name="fileInfo"></param>
        /// <returns></returns>
        Google.Apis.Drive.v2.Data.File UploadSingleFile(FileInfo fileInfo);

        /// <summary>
        /// Uploads a whole list of files to google drive
        /// </summary>
        /// <param name="filesystem"></param>
        /// <returns>List of files with references to google drive</returns>
        Dictionary<string, Google.Apis.Drive.v2.Data.File> UploadFilesWithDirectories(List<FileSystemInfo> filesystem, string password);

        /// <summary>
        /// Downloads a files content from google drive
        /// </summary>
        /// <param name="file">file in google drive</param>
        /// <returns>the content of a google file</returns>
        System.IO.Stream DownloadFile(Google.Apis.Drive.v2.Data.File file);

        /// <summary>
        /// Downloads a file with its url
        /// </summary>
        /// <param name="fileUrl">url where the file is located in google drive</param>
        /// <returns>filestream of file</returns>
        System.IO.Stream DownloadFile(string id);

        /// <summary>
        /// Adds a new file to backup folder
        /// </summary>
        /// <param name="parentId">Google id of parent folder</param>
        /// <param name="fileInfo">File which has to be uploaded</param>
        /// <returns></returns>
        Google.Apis.Drive.v2.Data.File UploadFileAddToBackupDirectory(string parentId, FileInfo fileInfo, string password);

        /// <summary>
        /// Creates a new folder in google drive
        /// </summary>
        /// <param name="parentId">Google id of parent folder</param>
        /// <param name="name">name of folder</param>
        /// <returns></returns>
        Google.Apis.Drive.v2.Data.File CreateFolderInDrive(string parentId, DirectoryInfo dInfo);

        /// <summary>
        /// Updates a file in google drive
        /// </summary>
        /// <param name="id">id of file in google drive</param>
        /// <param name="fInfo">file which has to be updated</param>
        void UpdateFileInDrive(string id, FileInfo fInfo, string password);

        /// <summary>
        /// Calls the google api an revokes the access from Drive
        /// </summary>
        void RevokeAccessToken();
    }
}
