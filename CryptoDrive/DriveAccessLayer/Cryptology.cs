﻿using FileAccessLayer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace FileAccessLayer
{
    public class Cryptology: ICryptology
    {
        private const int SizeOfBuffer = 1024 * 8;
        private const string Salt = "d5fg4df5sg4ds5fg45sdfg4";

        public FileInfo EncryptFile(System.IO.FileInfo fInfo, string password)
        {
            if(File.Exists(fInfo.FullName))
            {
                var input = new FileStream(fInfo.FullName, FileMode.Open, FileAccess.Read);
                var algorithm = new RijndaelManaged { KeySize = 256, BlockSize = 128 };

                var key = new Rfc2898DeriveBytes(password, Encoding.ASCII.GetBytes(Salt));

                algorithm.Key = key.GetBytes(algorithm.KeySize / 8);
                algorithm.IV = key.GetBytes(algorithm.BlockSize / 8);

                using (var encryptedStream = new CryptoStream(new FileStream(fInfo.FullName + "_encrypted", FileMode.OpenOrCreate, FileAccess.Write),
                    algorithm.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    CopyStream(input, encryptedStream);
                }
                return new FileInfo(fInfo.FullName + "_encrypted");
            }
            return null;
        }

        public FileInfo DecryptFile(System.IO.FileInfo fInfo, string password)
        {
            if (File.Exists(fInfo.FullName))
            {
                string pathOfDecryptedFile;
                var input = new FileStream(fInfo.FullName, FileMode.Open, FileAccess.Read);
                var algorithm = new RijndaelManaged { KeySize = 256, BlockSize = 128 };

                var key = new Rfc2898DeriveBytes(password, Encoding.ASCII.GetBytes(Salt));

                algorithm.Key = key.GetBytes(algorithm.KeySize / 8);
                algorithm.IV = key.GetBytes(algorithm.BlockSize / 8);

                try
                {
                    //delete _encrypted to restore original filename
                    pathOfDecryptedFile = fInfo.FullName.Replace("_encrypted", "");
                    using (var decryptedStream = new CryptoStream(new FileStream(pathOfDecryptedFile, FileMode.OpenOrCreate, FileAccess.Write),
                        algorithm.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        CopyStream(input, decryptedStream);
                    }
                }
                catch (CryptographicException ce)
                {
                    System.Diagnostics.EventLog appLog = new System.Diagnostics.EventLog();
                    appLog.Source = "CryptoDrive";
                    appLog.WriteEntry("wrong password to decrypt file" + ce.Message, System.Diagnostics.EventLogEntryType.Warning, 666);
                }

                return new FileInfo(fInfo.FullName);
            }
            return null;
        }

        /// <summary>
        /// Copy a filestream from input to output stream
        /// </summary>
        /// <param name="input"></param>
        /// <param name="output"></param>
        private static void CopyStream(Stream input, Stream output)
        {
            using (output)
            using (input)
            {
                byte[] buffer = new byte[SizeOfBuffer];
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    output.Write(buffer, 0, read);
                }
            }
        }
    }
}
