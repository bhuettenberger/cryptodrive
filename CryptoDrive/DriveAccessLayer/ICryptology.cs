﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace FileAccessLayer
{
    public interface ICryptology
    {
        /// <summary>
        /// Encrypts a file with AES Algorithm
        /// </summary>
        /// <param name="fInfo">file which has to be encrypted</param>
        /// <param name="password">required password</param>
        /// <returns>encrypted file</returns>
        FileInfo EncryptFile(FileInfo fInfo, string password);

        /// <summary>
        /// Decrypts a file
        /// </summary>
        /// <param name="fInfo">file which has to be decrypted</param>
        /// <param name="password">required password for decryption</param>
        /// <returns>decrypted file</returns>
        FileInfo DecryptFile(FileInfo fInfo, string password);
    }
}
