﻿using FileAccessLayer.XmlObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileAccessLayer
{
    public interface IXmlManager
    {
        /// <summary>
        /// serializes a list of gFiles to an Xml File
        /// </summary>
        /// <param name="filePath">path of the xml file to serialize</param>
        /// <param name="gFiles">List of google files</param>
        void SerializeToXml(string filePath, List<GoogleFile> gFiles);

        /// <summary>
        /// deserializes a xml into a list of google files
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns>List of google files</returns>
        List<GoogleFile> DeserializeFromXml(string filePath);
    }
}
