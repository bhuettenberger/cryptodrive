﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileAccessLayer
{
    public class Filemanager: IFileManager
    {
        public DirectoryInfo dInfo { get; set; }
        private List<FileInfo> files;

        //main list which contains files and directories
        private List<FileSystemInfo> directories;

        public Filemanager()
        {
            this.files = new List<FileInfo>();
            this.directories = new List<FileSystemInfo>();
        }
     
        public List<FileInfo> GetAllFilesOfDirectory()
        {
            GetAllFilesOfDirectoryWithSubFiles(this.dInfo);
            return files;
        }

        /// <summary>
        /// fills list files with filenames
        /// </summary>
        /// <param name="directoryInfo"></param>
        private void GetAllFilesOfDirectoryWithSubFiles(DirectoryInfo directoryInfo)
        {
            List<FileInfo> list = directoryInfo.GetFiles().ToList();

            files.AddRange(list);

            List<DirectoryInfo> directories = directoryInfo.GetDirectories().ToList();

            foreach (var directory in directories)
            {
                GetAllFilesOfDirectoryWithSubFiles(directory);
            }
        }

        public List<FileSystemInfo> GetAllFilesOfDirectoryWithFolders()
        {
            directories.Clear();
            GetAllFilesOfDirectoryWithDirectories(this.dInfo);
            return directories;
        }

        /// <summary>
        /// fills list directories with files an directories
        /// </summary>
        /// <param name="directoryInfo"></param>
        private void GetAllFilesOfDirectoryWithDirectories(DirectoryInfo directoryInfo)
        {
            List<DirectoryInfo> dList = directoryInfo.GetDirectories().ToList();
            List<FileInfo> fList = directoryInfo.GetFiles().ToList();

            directories.AddRange(dList);
            directories.AddRange(fList);

            foreach (var directory in dList)
            {
                GetAllFilesOfDirectoryWithDirectories(directory);
            }
        }

        public void CreateFileFromStream(string filePath, Stream stream)
        {
            using (var fileStream = File.Create(filePath))
            {
                stream.CopyTo(fileStream);
            }
        }


        public void CreateDirectory(string directoryPath)
        {
            if(!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }  
        }


        public void DeleteFile(string filePath)
        {
            File.Delete(filePath);
        }


        public List<FileInfo> GetAllFilesOfDirectory(DirectoryInfo dInfo)
        {
            if(Directory.Exists(dInfo.FullName))
            {
                return dInfo.GetFiles().ToList();
            }
            return new List<FileInfo>();
        }


        public bool DeleteGoogleAccessToken()
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            DirectoryInfo dInfo = new DirectoryInfo(path);
            path += "\\Google.Apis.Auth";

            if(Directory.Exists(path))
            {
                foreach (var file in dInfo.GetFiles())
                {
                    file.Delete();
                }
                return true;
            }
            return false;
        }
    }
}
