﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileAccessLayer.XmlObjects
{
    public class GoogleFile
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string FilePath { get; set; }

        public string DownloadUrl { get; set; }

        public DateTime? Created { get; set; }
    }
}
