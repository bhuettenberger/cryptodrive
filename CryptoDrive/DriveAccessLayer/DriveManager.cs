﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v2;
using Google.Apis.Services;
using Google.Apis.Drive.v2.Data;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;

namespace FileAccessLayer
{
    public class DriveManager: IDriveManager
    {
        public event UploadProgress Progress;
        private UserCredential credential;
        private IFileManager filemanager;
        private ICryptology crypto;
        public string clientId { get; set; }
        public string clientSecret { get; set; }
        public Dictionary<string, Google.Apis.Drive.v2.Data.File> gFiles { get; set; }

        private DirectoryInfo rootFolder;

        public DriveManager() { }

        public DriveManager(string clientId, string clientSecret)
        {
            this.clientId = clientId;
            this.clientSecret = clientSecret;
            this.gFiles = new Dictionary<string, Google.Apis.Drive.v2.Data.File>();
            this.crypto = new Cryptology();
            this.filemanager = new Filemanager();
        }

        /// <summary>
        /// Initializes the service instance to google drive
        /// </summary>
        /// <returns></returns>
        private DriveService InitializeDriveService()
        {
            credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
            new ClientSecrets
            {
                ClientId = clientId,
                ClientSecret = clientSecret,
            },
            new[] { DriveService.Scope.Drive },
            "user",
            CancellationToken.None).Result;


            // Create the service.
            var service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "CryptoDrive",
            });

            if (service == null)
            {
                throw new Exception("Verbindung zu Google Drive konnte nicht hergestellt werden. Bitte versuchen Sie es erneut!");
            }

            return service;
        }

        public Google.Apis.Drive.v2.Data.File UploadSingleFile(FileInfo fileInfo)
        {
            var service = InitializeDriveService();

            Google.Apis.Drive.v2.Data.File body = new Google.Apis.Drive.v2.Data.File();
            body.Title = fileInfo.Name;
            body.Description = fileInfo.Extension;
            body.MimeType = "text/plain";


            byte[] byteArray = System.IO.File.ReadAllBytes(fileInfo.FullName);
            System.IO.MemoryStream stream = new System.IO.MemoryStream(byteArray);

            FilesResource.InsertMediaUpload request = service.Files.Insert(body, stream, "text/plain");
            request.Upload();

            Google.Apis.Drive.v2.Data.File file = request.ResponseBody;

            return file;
        }

        public Dictionary<string, Google.Apis.Drive.v2.Data.File> UploadFilesWithDirectories(List<FileSystemInfo> filesystem, string password)
        {
            double count = 1;
            double percent;
            string id = "";
            gFiles.Clear();
            if (filesystem != null && filesystem.Count > 0)
            {
                //store root folder path
                if (filesystem.ElementAt(0).Attributes.ToString().IndexOf("Directory") != -1)
                {
                    rootFolder = (DirectoryInfo)filesystem.ElementAt(0);
                    rootFolder = rootFolder.Parent;
                }

                //create backup folder with timestamp
                Google.Apis.Drive.v2.Data.File body = new Google.Apis.Drive.v2.Data.File();
                body.Title = "Backup";
                body.Description = "Backup from the " + DateTime.Now;
                body.MimeType = "application/vnd.google-apps.folder";

                var service = InitializeDriveService();

                var backupFolder = service.Files.Insert(body).Execute();

                gFiles.Add(rootFolder.FullName, backupFolder);

                //upload each file with same filestructure as source
                foreach (var fileElement in filesystem)
                {
                    percent = count / filesystem.Count;
                    if (fileElement.Attributes.ToString().IndexOf("Directory") != -1) //create directory
                    {
                        DirectoryInfo tempDirectory = (DirectoryInfo)fileElement;

                        //parent folder is root folder so choose backupfolder as destination
                        if(tempDirectory.Parent.FullName.Equals(rootFolder.FullName))
                        {
                            id = backupFolder.Id;
                        }
                        else 
                        {
                            //look for folder in dictionary
                            var gFile = (from file in gFiles
                                         where file.Key == tempDirectory.Parent.FullName
                                         select file).SingleOrDefault();

                            if(gFile.Value != null)
                            {
                                id = gFile.Value.Id;
                            }
                        }

                        //create directory and upoad it to drive
                        Google.Apis.Drive.v2.Data.File newFolder = new Google.Apis.Drive.v2.Data.File();
                        newFolder.Title = tempDirectory.Name;
                        newFolder.MimeType = "application/vnd.google-apps.folder";
                        //apply parent folder
                        newFolder.Parents = new List<ParentReference>() { new ParentReference() { Id = id } };

                        var uploadedFolder = service.Files.Insert(newFolder).Execute();
                        gFiles.Add(tempDirectory.FullName, uploadedFolder);
                    }
                    else //upload normal file
                    {
                        FileInfo unencryptedFile = (FileInfo)fileElement;

                        //encrypt file with password
                        FileInfo tempFile = crypto.EncryptFile(unencryptedFile, password);

                        if (tempFile != null)
                        {
                            //parent folder is root folder so choose backupfolder as destination
                            if (tempFile.Directory.FullName.Equals(rootFolder.FullName))
                            {
                                id = backupFolder.Id;
                            }
                            else
                            {
                                //look for folder id in dictionary
                                var gFile = (from file in gFiles
                                             where file.Key == tempFile.Directory.FullName
                                             select file).SingleOrDefault();

                                if (gFile.Value != null)
                                {
                                    id = gFile.Value.Id;
                                }
                            }

                            //upload file to google drive
                            Google.Apis.Drive.v2.Data.File newFile = new Google.Apis.Drive.v2.Data.File();
                            newFile.Title = tempFile.Name;
                            newFile.Description = tempFile.Extension;
                            newFile.MimeType = "application/octet-stream";
                            //apply parent folder
                            newFile.Parents = new List<ParentReference>() { new ParentReference() { Id = id } };

                            //read file into bytestream
                            byte[] byteArray = System.IO.File.ReadAllBytes(tempFile.FullName);
                            System.IO.MemoryStream stream = new System.IO.MemoryStream(byteArray);

                            //upload file
                            FilesResource.InsertMediaUpload request = service.Files.Insert(newFile, stream, "application/octet-stream");
                            var uploadedItem = request.Upload();

                            //get file id from request
                            newFile = request.ResponseBody;

                            gFiles.Add(tempFile.FullName, newFile);

                            //delete encrypted file from directory
                            filemanager.DeleteFile(tempFile.FullName);
                        }
                    }
                    FireProgress(percent);
                    count++;
                }
            }
            
            return gFiles;
        }

        //fire event
        private void FireProgress(double percent)
        {
            if(Progress != null)
            {
                Progress(percent * 100);
            }
        }

        public Stream DownloadFile(Google.Apis.Drive.v2.Data.File file)
        {
            if(!String.IsNullOrEmpty(file.DownloadUrl))
            {
                var service = InitializeDriveService();
                var stream = service.HttpClient.GetStreamAsync(file.DownloadUrl);   
              
                while(!stream.IsCompleted)
                {
                    Thread.Sleep(1000);
                }

                return stream.Result;
            }
            return null;
        }


        public Google.Apis.Drive.v2.Data.File UploadFileAddToBackupDirectory(string parentId, FileInfo fileInfo, string password)
        {
            if(parentId.Length > 0)
            {
                var service = InitializeDriveService();

                //encrypt file with password
                FileInfo tempFile = crypto.EncryptFile(fileInfo, password);

                //upload file to google drive
                Google.Apis.Drive.v2.Data.File newFile = new Google.Apis.Drive.v2.Data.File();

                if (tempFile != null)
                {
                    newFile.Title = tempFile.Name;
                    newFile.Description = fileInfo.Extension;
                    newFile.MimeType = "application/octet-stream";
                    //apply parent folder
                    newFile.Parents = new List<ParentReference>() { new ParentReference() { Id = parentId } };

                
                    //read file into bytestream
                    byte[] byteArray = System.IO.File.ReadAllBytes(tempFile.FullName);
                    System.IO.MemoryStream stream = new System.IO.MemoryStream(byteArray);

                    //upload file
                    FilesResource.InsertMediaUpload request = service.Files.Insert(newFile, stream, "application/octet-stream");
                    var uploadedItem = request.Upload();

                    //get file id from request
                    newFile = request.ResponseBody;

                    filemanager.DeleteFile(tempFile.FullName);
                }
                return newFile;
            }
            else
            {
                throw new ArgumentException("ParentId must not be null");
            }
        }

        public Google.Apis.Drive.v2.Data.File CreateFolderInDrive(string parentId, DirectoryInfo dInfo)
        {
            var service = InitializeDriveService();
            //create directory and upoad it to drive
            Google.Apis.Drive.v2.Data.File newFolder = new Google.Apis.Drive.v2.Data.File();
            newFolder.Title = dInfo.Name;
            newFolder.MimeType = "application/vnd.google-apps.folder";

            if (parentId.Length > 0)
            {
                //apply parent folder
                newFolder.Parents = new List<ParentReference>() { new ParentReference() { Id = parentId } };
            }
            var uploadedFolder = service.Files.Insert(newFolder).Execute();

            return uploadedFolder;
        }

        public void UpdateFileInDrive(string id, FileInfo fInfo, string password)
        {
            var service = this.InitializeDriveService();
            FileInfo tempFile = crypto.EncryptFile(fInfo, password);

            if (tempFile != null)
            {
                //get file from google drive
                var gFile = service.Files.Get(id).Execute();
                byte[] byteArray = System.IO.File.ReadAllBytes(tempFile.FullName);
                System.IO.MemoryStream stream = new System.IO.MemoryStream(byteArray);

                //update file in google drive
                FilesResource.UpdateMediaUpload request = service.Files.Update(gFile, id, stream, gFile.MimeType);
                request.Upload();

                filemanager.DeleteFile(tempFile.FullName);
            }
        }

        public Stream DownloadFile(string id)
        {
            if (!String.IsNullOrEmpty(id))
            {
                var service = InitializeDriveService();
                var file = service.Files.Get(id).Execute();
                
                var stream = service.HttpClient.GetStreamAsync(file.DownloadUrl);

                while (!stream.IsCompleted)
                {
                    Thread.Sleep(1000);
                }

                return stream.Result;
            }
            return null;
        }


        public void RevokeAccessToken()
        {
            if(credential == null){
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    new ClientSecrets
                    {
                        ClientId = clientId,
                        ClientSecret = clientSecret,
                    },
                    new[] { DriveService.Scope.Drive },
                    "user",
                    CancellationToken.None).Result;
            }

            try
            {
                string url = "https://accounts.google.com/o/oauth2/revoke?token=" + credential.Token.RefreshToken;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                if ((int)response.StatusCode != 200)
                {
                    throw new Exception("Drive Authentifizierung konnte nicht aufgehoben werden!");
                }
                
            }
            catch (WebException wex)
            {
                throw new Exception("Die Anwendung konnte nicht abgemeldet werden. Bitte wiederholen Sie den Vorgang");
            }
        }
    }
}
