﻿using FileAccessLayer.XmlObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FileAccessLayer
{
    public class XmlManager: IXmlManager
    {
        private XmlSerializer serializer;

        public XmlManager()
        {
            serializer = new XmlSerializer(typeof(List<GoogleFile>));
        }

        public void SerializeToXml(string filePath, List<GoogleFile> gFiles)
        {
            if(gFiles != null)
            {
                TextWriter textWriter = new StreamWriter(filePath);
                serializer.Serialize(textWriter, gFiles);
                textWriter.Close();
            }
            else
            {
                throw new ArgumentException("no files to serialize");
            }
        }

        public List<GoogleFile> DeserializeFromXml(string filePath)
        {
            List<GoogleFile> gFiles = new List<GoogleFile>();

            if (File.Exists(filePath))
            {
                TextReader textReader = new StreamReader(filePath);
                gFiles = (List<GoogleFile>)serializer.Deserialize(textReader);
                textReader.Close();
            }
            else
            {
                throw new ArgumentException("Xml file doesn't exist");
            }

            return gFiles;
        }
    }
}
