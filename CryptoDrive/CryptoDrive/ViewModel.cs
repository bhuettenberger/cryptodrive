﻿using BusinessLayer;
using FileAccessLayer;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;

namespace CryptoDrive
{
    public class ViewModel: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private Thread backupThread;
        private IDataManager dataManager;
        private ICommand abort;
        private ICommand createBackup;
        private ICommand backupFolderCommand;
        private List<FileInfo> fInfos;
        private List<string> xmlFiles;
        private List<KeyValuePair<string, string>> listOfBackupFiles;
        private int progress;
        private string path;
        private string xmlPath;
        private string visible = "Visible";
        private string message;
        private string storagePath;
        private string selectedItem;
        public string Password { get; set; }

        public ViewModel()
        {
            var container = new UnityContainer();
            container.RegisterType<IDriveManager, DriveManager>(new InjectionConstructor("620997274493-numddga8unvdlquhagip0nsevhm13a01.apps.googleusercontent.com", "aHplhk_1i1pAeg7SMIWXeDqg"));
            container.RegisterType<ICryptology, Cryptology>();
            container.RegisterType<IFileManager, Filemanager>();
            container.RegisterType<IXmlManager, XmlManager>();

            dataManager = container.Resolve<DataManager>();
            dataManager.Progress += dataManager_Progress;
            dataManager.ErrorMessage += dataManager_ErrorMessage;
            xmlFiles = new List<string>();
            listOfBackupFiles = new List<KeyValuePair<string, string>>();

            Init();
        }

        

        //get progress of file upload
        private void dataManager_Progress(double percent)
        {
            Progress = (int)percent;
        }

        void dataManager_ErrorMessage(string message)
        {
            System.Windows.MessageBox.Show(message);
            Message = message;
        }

        public List<string> XmlFiles
        {
            get
            {
                return xmlFiles;
            }
            set
            {
                xmlFiles = value;
                OnPropertyChanged("XmlFiles");
            }
        }

        public string SelectedItem
        {
            get
            {
                return selectedItem;
            }
            set
            {
                selectedItem = value;
                OnPropertyChanged("SelectedItem");
            }
        }

        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                message = value;
                OnPropertyChanged("Message");
            }
        }

        public string Visible
        {
            get
            {
                return visible;
            }
            set
            {
                visible = value;
                OnPropertyChanged("Visible");
            }
        }

        public int Progress
        {
            get
            {
                return progress;
            }
            set
            {
                progress = value;
                OnPropertyChanged("Progress");
                if(progress > 99.9999)
                {
                    Visible = "Visible";
                    System.Windows.MessageBox.Show("Operation erfolgreich durchgeführt!");
                }
            }
        }

        public string Path { 
            get
            {
                return path;
            }
            set
            {
                path = value;
                OnPropertyChanged("Path");
            }
        }

        public string XmlPath
        {
            get
            {
                return xmlPath;
            }
            set
            {
                xmlPath = value;
                OnPropertyChanged("XmlPath");
            }
        }

        public ICommand Abort
        {
            get
            {
                if(abort == null)
                {
                    abort = new RelayCommand(param => StopThread());
                }
                return abort;
            }
        }

        private void StopThread()
        {
            if(backupThread != null && backupThread.IsAlive)
            {
                backupThread.Abort();
            }
            Visible = "Visible";
        }

        public ICommand CreateBackup
        {
            get
            {
                if(createBackup == null)
                {
                    createBackup = new RelayCommand(param => StartBackup(param));
                }
                return createBackup;
            }
        }

        public ICommand RestoreBackup
        {
            get
            {
                if (createBackup == null)
                {
                    createBackup = new RelayCommand(param => RestoreFiles(param));
                }
                return createBackup;
            }
        }

        private void RestoreFiles(object param)
        {
            int position = 0;
            string substring = "";
            //get password from passwordbox
            var passwordBox = param as PasswordBox;
            Password = passwordBox.Password;

            //location of the location
            string tempPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            xmlPath = System.IO.Path.GetFullPath(tempPath);
            //location of the file xml
            xmlPath = xmlPath.Replace(System.IO.Path.GetFileName(tempPath), "Backups\\");

            //Add name of selected backupfile
            foreach (var backupFile in listOfBackupFiles)
            {
                if(selectedItem == backupFile.Value)
                {
                    position = backupFile.Key.LastIndexOf("\\");
                    substring = backupFile.Key.Substring(position + 1);
                    xmlPath += substring;
                }
            }

            Visible = "Hidden";

            try
            {
                if (path == null || !Directory.Exists(path) || selectedItem == null)
                {
                    throw new ArgumentException("Eingabeparameter ungültig! Bitte überprüfen Sie die Passwortlänge und den Dateipfad");
                }
                //start Backup async
                backupThread = new Thread(() => dataManager.RestoreBackup(new DirectoryInfo(path), Password, xmlPath));
                backupThread.Start();
            }
            catch (Exception e)
            {
                dataManager_ErrorMessage(e.Message);
            }
        }

        private void StartBackup(object param)
        {
            //get password from passwordbox
            var passwordBox = param as PasswordBox;
            Password = passwordBox.Password;

            //location of the location
            string tempPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            xmlPath = System.IO.Path.GetFullPath(tempPath);
            //location of the file xml
            xmlPath = xmlPath.Replace(System.IO.Path.GetFileName(tempPath), "Backups\\");
            xmlPath += Guid.NewGuid().ToString() + ".xml";

            Visible = "Hidden";

            try
            {
                if(path == null || !Directory.Exists(path))
                {
                    throw new ArgumentException("Eingabeparameter ungültig! Bitte überprüfen Sie die Passwortlänge und den Dateipfad");
                }
                //start Backup async
                backupThread = new Thread(() => dataManager.CreateBackup(new DirectoryInfo(path), Password, xmlPath));
                backupThread.Start();
            }
            catch (Exception e)
            {
                dataManager_ErrorMessage(e.Message);
            }
        }

        public ICommand BackupFolderCommand {
            get
            {
                if(backupFolderCommand == null)
                {
                    backupFolderCommand = new RelayCommand(param => SetBackupFolder());
                }
                return backupFolderCommand;
            }
        }

        private void SetBackupFolder()
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            System.Windows.Forms.DialogResult result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                this.Path = dialog.SelectedPath.ToString();
            }
        }

        //initialises list of backups
        private void Init()
        {
            //get all backup files
            storagePath = System.IO.Path.GetFullPath(System.Reflection.Assembly.GetExecutingAssembly().Location);
            storagePath = storagePath.Replace(System.IO.Path.GetFileName(storagePath), "Backups\\");
            listOfBackupFiles = dataManager.GetAllFilesOfFolder(new DirectoryInfo(storagePath));

            foreach (var backupFile in listOfBackupFiles)
            {
                xmlFiles.Add(backupFile.Value);
            }
        }

        // Create the OnPropertyChanged method to raise the event
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
