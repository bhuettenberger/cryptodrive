﻿using BusinessLayer;
using FileAccessLayer;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CryptoDrive
{
    /// <summary>
    /// Interaction logic for Main.xaml
    /// </summary>
    public partial class Main : Window
    {
        IDataManager dataManager;
        public Main()
        {
            InitializeComponent();
            var container = new UnityContainer();
            container.RegisterType<IDriveManager, DriveManager>(new InjectionConstructor("620997274493-numddga8unvdlquhagip0nsevhm13a01.apps.googleusercontent.com", "aHplhk_1i1pAeg7SMIWXeDqg"));
            container.RegisterType<ICryptology, Cryptology>();
            container.RegisterType<IFileManager, Filemanager>();
            container.RegisterType<IXmlManager, XmlManager>();

            dataManager = container.Resolve<DataManager>();
        }

        //starts the backup window
        private void OpenBackup(object sender, RoutedEventArgs e)
        {
            Window window = new CryptoDrive.MainWindow();
            window.Show();
            this.Close();
        }

        private void Restore(object sender, RoutedEventArgs e)
        {
            Window window = new CryptoDrive.RestoreWindow();
            window.Show();
            this.Close();
        }

        private void RevokeAccess(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Wollen Sie den Zugriff auf Google Drive wirklich aufheben? Achtung Backups können eventuell nicht mehr wiederhergestellt werden!",
                "Anwendungsabmeldung", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                try
                {
                    dataManager.RevokeAccessOfApplication();
                    MessageBox.Show("Die Anwendung wurde von Google Drive angemeldet.", "Abmeldungsinfo");
                }
                catch
                {
                    MessageBox.Show("Die Anwendung konnte nicht abgemeldet werden!", "Abmeldungsfehler");
                }
            }
        }
    }
}
