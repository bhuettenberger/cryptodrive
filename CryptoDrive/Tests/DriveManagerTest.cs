﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using FileAccessLayer;
using System.Collections.Generic;

using Google.Apis.Drive.v2.Data;

namespace Tests
{
    [TestClass]
    public class DriveManagerTest
    {
        [TestMethod]
        [TestCategory("DriveTests (FileAccessLayer)")]
        public void UploadFile()
        {
            FileInfo fileInfo = new FileInfo("C:\\Users\\Greyfox\\Downloads\\document.txt");

            IDriveManager dm = new DriveManager("620997274493-numddga8unvdlquhagip0nsevhm13a01.apps.googleusercontent.com", "aHplhk_1i1pAeg7SMIWXeDqg");

            Google.Apis.Drive.v2.Data.File file = dm.UploadSingleFile(fileInfo);

            Assert.IsTrue(file != null);
        }

        [TestMethod]
        [TestCategory("DriveTests (FileAccessLayer)")]
        public void CreateBackupOfDirectoryInDrive()
        {
            IDriveManager dm = new DriveManager("620997274493-numddga8unvdlquhagip0nsevhm13a01.apps.googleusercontent.com", "aHplhk_1i1pAeg7SMIWXeDqg");
            IFileManager fm = new Filemanager();
            fm.dInfo = new DirectoryInfo("C:\\Users\\Greyfox\\Documents\\root");

            List<FileSystemInfo> filestructure = fm.GetAllFilesOfDirectoryWithFolders();

            Dictionary<string, Google.Apis.Drive.v2.Data.File> gFiles = dm.UploadFilesWithDirectories(filestructure, "password");

            Assert.IsTrue(gFiles.Count != 0);
        }

        [TestMethod]
        [TestCategory("DriveTests (FileAccessLayer)")]
        public void DownloadFilestreamsFromDrive()
        {
            string directory = "";
            string file = "";
            Stream stream;
            int count = 0;
            IDriveManager dm = new DriveManager("620997274493-numddga8unvdlquhagip0nsevhm13a01.apps.googleusercontent.com", "aHplhk_1i1pAeg7SMIWXeDqg");
            IFileManager fm = new Filemanager();
            fm.dInfo = new DirectoryInfo("C:\\Users\\Greyfox\\Documents\\root");

            List<FileSystemInfo> filestructure = fm.GetAllFilesOfDirectoryWithFolders();

            Dictionary<string, Google.Apis.Drive.v2.Data.File> gFiles = dm.UploadFilesWithDirectories(filestructure, "password");

            foreach (var gFile in gFiles)
            {
                stream = dm.DownloadFile(gFile.Value);
                if(stream != null) //create file
                {
                    count++;
                    file = gFile.Key.Replace("root", "backup");
                    fm.CreateFileFromStream(file, stream);
                }
                else //create directory
                {
                    directory = gFile.Key.Replace("root", "backup");
                    fm.CreateDirectory(directory);
                }
            }

            Assert.IsTrue(count != 0);
        }

        [TestMethod]
        [TestCategory("DriveTests (FileAccessLayer)")]
        public void UploadFolderToDrive()
        {
            IDriveManager dm = new DriveManager("620997274493-numddga8unvdlquhagip0nsevhm13a01.apps.googleusercontent.com", "aHplhk_1i1pAeg7SMIWXeDqg");
            DirectoryInfo dInfo = new DirectoryInfo("C:\\Users\\Greyfox\\Documents\\root");

            var gFile = dm.CreateFolderInDrive("", dInfo);

            Assert.IsTrue(gFile.Id.Length > 0);
        }

        [TestMethod]
        [TestCategory("DriveTests (FileAccessLayer)")]
        public void UploadFileIntoFolder()
        {
            IDriveManager dm = new DriveManager("620997274493-numddga8unvdlquhagip0nsevhm13a01.apps.googleusercontent.com", "aHplhk_1i1pAeg7SMIWXeDqg");
            DirectoryInfo dInfo = new DirectoryInfo("C:\\Users\\Greyfox\\Documents\\root");
            FileInfo fInfo = new FileInfo("C:\\Users\\Greyfox\\Documents\\root\\Test.txt");

            var dFile = dm.CreateFolderInDrive("", dInfo);
            var gFile = dm.UploadFileAddToBackupDirectory(dFile.Id, fInfo, "password");

            Assert.IsNotNull(gFile);
        }

        [TestMethod]
        [TestCategory("DriveTests (FileAccessLayer)")]
        public void DeleteAccessToken()
        {
            IDriveManager dm = new DriveManager("620997274493-numddga8unvdlquhagip0nsevhm13a01.apps.googleusercontent.com", "aHplhk_1i1pAeg7SMIWXeDqg");
            dm.RevokeAccessToken();
        }
    }
}

