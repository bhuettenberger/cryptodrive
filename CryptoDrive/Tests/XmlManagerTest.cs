﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FileAccessLayer;
using System.IO;
using System.Collections.Generic;
using FileAccessLayer.XmlObjects;

namespace Tests
{
    [TestClass]
    public class XmlManagerTest
    {
        [TestMethod]
        [TestCategory("XmlTests (FileAccessLayer)")]
        public void SerializeGfilesToXml()
        {
            IDriveManager dm = new DriveManager("620997274493-numddga8unvdlquhagip0nsevhm13a01.apps.googleusercontent.com", "aHplhk_1i1pAeg7SMIWXeDqg");
            IFileManager fm = new Filemanager();
            fm.dInfo = new DirectoryInfo("C:\\Users\\Greyfox\\Documents\\root");
            IXmlManager xm = new XmlManager();

            List<GoogleFile> serializedFiles = new List<GoogleFile>();

            List<FileSystemInfo> filestructure = fm.GetAllFilesOfDirectoryWithFolders();

            Dictionary<string, Google.Apis.Drive.v2.Data.File> gFiles = dm.UploadFilesWithDirectories(filestructure, "password");

            foreach (var gFile in gFiles)
            {
                var googleFile = new GoogleFile();
                googleFile.Id = gFile.Value.Id;
                googleFile.DownloadUrl = gFile.Value.DownloadUrl;
                googleFile.FilePath = gFile.Key;
                googleFile.Name = gFile.Value.Title;
                googleFile.Created = gFile.Value.CreatedDate;
                serializedFiles.Add(googleFile);
            }

            xm.SerializeToXml("C:\\Users\\Greyfox\\Documents\\GoogleFiles.xml", serializedFiles);

            Assert.IsTrue(serializedFiles.Count > 0);
        }

        [TestMethod]
        [TestCategory("XmlTests (FileAccessLayer)")]
        public void DeserializeFromXml()
        {
            IXmlManager xm = new XmlManager();
            List<GoogleFile> deserializedFiles = xm.DeserializeFromXml("C:\\Users\\Greyfox\\Documents\\GoogleFiles.xml");
            Assert.IsTrue(deserializedFiles.Count > 0);
        }
    }
}
