﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Practices.Unity;
using FileAccessLayer;
using BusinessLayer;
using System.IO;

namespace Tests
{
    [TestClass]
    public class DataManagerTest
    {
        [TestMethod]
        [TestCategory("DataManager (Business Layer)")]
        public void UploadFolder()
        {
            DirectoryInfo dInfo = new DirectoryInfo("C:\\Users\\Greyfox\\Documents\\root");

            var container = new UnityContainer();
            container.RegisterType<IDriveManager, DriveManager>(new InjectionConstructor("620997274493-numddga8unvdlquhagip0nsevhm13a01.apps.googleusercontent.com", "aHplhk_1i1pAeg7SMIWXeDqg"));
            container.RegisterType<ICryptology, Cryptology>();
            container.RegisterType<IFileManager, Filemanager>();
            container.RegisterType<IXmlManager, XmlManager>();

            var dataManager = container.Resolve<DataManager>();

            dataManager.CreateBackup(dInfo, "password", "C:\\Users\\Greyfox\\Documents\\backupFiles.xml");

            Assert.IsTrue(true);
        }

        [TestMethod]
        [TestCategory("DataManager (Business Layer)")]
        public void RestoreFolder()
        {
            DirectoryInfo dInfo = new DirectoryInfo("C:\\Users\\Greyfox\\Documents\\restore");

            var container = new UnityContainer();
            container.RegisterType<IDriveManager, DriveManager>(new InjectionConstructor("620997274493-numddga8unvdlquhagip0nsevhm13a01.apps.googleusercontent.com", "aHplhk_1i1pAeg7SMIWXeDqg"));
            container.RegisterType<ICryptology, Cryptology>();
            container.RegisterType<IFileManager, Filemanager>();
            container.RegisterType<IXmlManager, XmlManager>();

            var dataManager = container.Resolve<DataManager>();

            dataManager.RestoreBackup(dInfo, "password", "C:\\Users\\Greyfox\\Documents\\backupFiles.xml");

            Assert.IsTrue(true);
        }
    }
}
