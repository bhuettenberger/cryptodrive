﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using FileAccessLayer;

namespace Tests
{
    [TestClass]
    public class CryptologyTest
    {
        [TestMethod]
        [TestCategory("CryptoTests (FileAccessLayer)")]
        public void EncryptFile()
        {
            FileInfo fInfo = new FileInfo("C:/Users/Greyfox/Downloads/document.txt");
            ICryptology crypt = new Cryptology();

            FileInfo encryptedFile = crypt.EncryptFile(fInfo, "password");

            Assert.AreNotEqual(fInfo.FullName, encryptedFile.FullName);
        }

        [TestMethod]
        [TestCategory("CryptoTests (FileAccessLayer)")]
        public void DecryptFile()
        {
            FileInfo fInfo = new FileInfo("C:/Users/Greyfox/Downloads/document.txt_encrypted");
            ICryptology crypt = new Cryptology();

            FileInfo decryptedFile = crypt.DecryptFile(fInfo, "password");

            Assert.AreNotEqual(fInfo.FullName, decryptedFile.FullName);
        }
    }
}
