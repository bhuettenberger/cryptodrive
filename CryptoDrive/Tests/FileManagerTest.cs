﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using FileAccessLayer;
using System.Collections.Generic;

namespace Tests
{
    [TestClass]
    public class FileManagerTest
    {
        [TestMethod]
        [TestCategory("FileTests (FileAccessLayer)")]
        public void GetAllFilesOfDirectory()
        {
            DirectoryInfo dInfo = new DirectoryInfo("C:\\Users\\Greyfox\\Downloads");
            IFileManager fManager = new Filemanager();
            fManager.dInfo = dInfo;

            List<FileInfo> files = fManager.GetAllFilesOfDirectory();

            Assert.IsTrue(files.Count > 0);
        }

        [TestMethod]
        [TestCategory("FileTests (FileAccessLayer)")]
        public void GetCompleteDirectoryStructureFromFilesystem()
        {
            DirectoryInfo dInfo = new DirectoryInfo("C:\\Users\\Greyfox\\Downloads");
            IFileManager fManager = new Filemanager();
            fManager.dInfo = dInfo;

            List<FileSystemInfo> directoryStructure = fManager.GetAllFilesOfDirectoryWithFolders();

            Assert.IsTrue(directoryStructure.Count > 0);
        }

        [TestMethod]
        [TestCategory("FileTests (FileAccessLayer)")]
        public void DeleteFile()
        {
            DirectoryInfo dInfo = new DirectoryInfo("C:\\Users\\Greyfox\\Downloads");
            FileInfo fInfo = new FileInfo("C:\\Users\\Greyfox\\Downloads\\document.txt_encrypted");
            IFileManager fManager = new Filemanager();
            fManager.dInfo = dInfo;

            fManager.DeleteFile(fInfo.FullName);
            Assert.IsFalse(fInfo.Exists);
        }

        [TestMethod]
        [TestCategory("FileTests (FileAccessLayer)")]
        public void DeleteGoogleAccessToken()
        {
            IFileManager fManager = new Filemanager();

            Assert.IsTrue(fManager.DeleteGoogleAccessToken());
        }
    }
}
